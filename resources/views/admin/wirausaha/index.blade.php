@extends('admin.layouts.app')
@section('content')
<div class="right_col" role="main">
    <div class="">
    	<div class="page-title">
        	<div class="title_left">
                <h3> Wirausaha Sidomulyo</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
            	<div class="x_panel">
                	<div class="x_title">
                    <button style="margin-left:970px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah">Tambah</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form role="form" method="POST" action="{{route('wirausahaAdmin.store')}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Wirausaha</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="foto">Foto</label>
                                            <input type="file" class="form-control-file" id="foto" name="foto">
                                        </div>
                                        <div class="form-group">
                                            <label for="namaWirausaha">Nama Wirausaha</label>
                                            <input type="text" class="form-control" id="namaWirausaha" placeholder="Nama Wirausaha" name="namaWirausaha">
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi">Deskripsi</label>
                                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"></textarea>
                                        </div>       
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                        @foreach ($wirausaha as $key => $wirausahaa)
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="{{asset('assets/images/wirausaha/'.$wirausahaa->foto)}}" alt="image" />
                                        <div class="mask">
                                            <p>{{ $wirausahaa->namaWirausaha }}</p>
                                            <div class="tools tools-bottom">
                                                <form action="{{route('wirausahaAdmin.delete', $wirausahaa->id)}}" method="POST" style="display: inline-block;">
                                                    {{ csrf_field() }}
                                                    <a data-toggle="modal" data-target="#ubah{{ $wirausahaa->id }}" class="btn btn-primary btn-xs">ubah</a>
                                                    <button class="btn btn-danger" onclick="return confirm('Delete?')">HAPUS</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <p>{{ $wirausahaa->deskripsi }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="ubah{{ $wirausahaa->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form role="form" method="POST" action="{{route('wirausahaAdmin.update',$wirausahaa->id)}}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ubah Wirausaha</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="foto">Foto</label>
                                                    <input type="file" class="form-control-file" id="foto" name="foto">
                                                </div>
                                                <div class="form-group">
                                                    <label for="namaWirausaha">Nama Wisata</label>
                                                    <textarea class="form-control" id="namaWirausaha" name="namaWirausaha" rows="1" placeholder="Nama Dusun" required>{{ $wirausahaa->namaWirausaha }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="deskripsi">Deskripsi</label>
                                                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" placeholder="Deskripsi" required>{{ $wirausahaa->deskripsi }}</textarea>
                                                </div>       
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        {{ $wirausaha->links() }}
                    </div>
        	    </div>
    	    </div>
        </div>
    </div>
</div>
@endsection