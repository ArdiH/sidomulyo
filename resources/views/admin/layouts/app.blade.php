<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Sidomulyo </title>
    <link href="{{asset('assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/admin/build/css/custom.min.css')}}" rel="stylesheet">
</head>
@yield('extrahead')
<body class="nav-md">
    <div class="container body">
    	<div class="main_container">
        	<div class="col-md-3 left_col menu_fixed">
            	<div class="left_col scroll-view">
                    <br /><br /><br />
                	<div class="navbar nav_title" style="border: 0;">
                        <center><a href="" class="site_title"><span>SIDOMULYO</span></a></center>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    	<div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="{{route('admin.home')}}"><i class="fa fa-home"></i>Beranda</a></li>
                                <li><a href="{{route('galeryAdmin.index')}}"><i class="fa fa-image"></i> Galery </a></li>
                                <li><a href="{{route('wisataAdmin.index')}}"><i class="fa fa-plane"></i>Wisata</a></li>
                                <li><a href="{{route('wirausahaAdmin.index')}}"><i class="fa fa-shopping-cart"></i>Wirausaha</a></li>
                                <li><a href="{{route('ubahPasswordAdmin.index')}}"><i class="fa fa-user"></i>Ganti Password</a></li>
                                <li>
                                    <a data-toggle="tooltip" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-placement="top" title="Keluar"><i class="fa fa-power-off"></i>Keluar</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidde">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-user"> Admin</i>
                                </a>
                            </li>
                            <li role="presentation" class="dropdown">
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                    <li>
                                        <a>
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                                <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                                <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                        	<span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                            	<span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a>
                                            <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                                            <span>
                                            <span>John Smith</span>
                                                <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                                Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="text-center">
                                        	<a>
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            @yield('content')
            <footer>
                <div class="pull-right">
                    
                </div>
                <div class="clearfix"></div>
			</footer>
        </div>
    </div>
    <script src="{{asset('assets/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/fastclick/lib/fastclick.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/nprogress/nprogress.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{asset('assets/admin/build/js/custom.min.js')}}"></script>
	@yield('extrascripts')
</body>
</html>