<?php

namespace App\Repositories;

use App\Models\Wirausaha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WirausahaRepository{

    private $model;

    public function __construct(Wirausaha $model){

        $this->model = $model;

    }

    public function get($pagination = null, $with = null){
        $wirausaha = $this->model
            ->when($with, function ($query) use ($with) {
                return $query->with($with);
            });

        if ($pagination) {
            return $wirausaha->paginate(10);
        }

        return $wirausaha->get();
    }


}
