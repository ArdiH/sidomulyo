<?php

namespace App\Repositories;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRepository{

    private $model;

    public function __construct(User $model){

        $this->model = $model;

    }

    public function get($pagination = null, $with = null){
        $user = $this->model
            ->when($with, function ($query) use ($with) {
                return $query->with($with);
            });

        if ($pagination) {
            return $user->paginate(10);
        }

        return $user->get();
    }


}
