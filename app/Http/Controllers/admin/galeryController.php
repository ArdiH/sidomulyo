<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Galery;
use App\Repositories\GaleryRepository;

use File;

class galeryController extends Controller{

    private $GaleryRepository;

    public function __construct(GaleryRepository $GaleryRepository){

        $this->GaleryRepository = $GaleryRepository;

    }

    public function index(Request $request){
        $page = $request->get('page', 1);
        $limit = 10;

        $galery = $this->GaleryRepository->get($limit);
        $data = [
            'no' => ($page - 1) * $limit,
            'galery' => $galery
        ];
        return view('admin/galery/index',$data);

    }

    public function store(Request $request){
        $validate = [
            'foto' => 'required|image|mimes:jpeg,png,jpg',
            'tanggal' => 'required|max:100',
            'namaKegiatan' => 'required|max:100',
            'deskripsi' => 'required|max:255',
          
        ];

        $this->validate(request(), $validate);
        $image = $request->file('foto');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('assets/images/galery'), $imageName);
        $detail = new Galery();
        $detail->tanggal = $request->get('tanggal');
        $detail->namaKegiatan = $request->get('namaKegiatan');
        $detail->deskripsi = $request->get('deskripsi');
        $detail->foto = $imageName;
        $detail->save();
        return redirect()->route('galeryAdmin.index')->with('success', 'Successfully created.');

    }

    public function update(Request $request, $id){

        $detail = Galery::where('id', $id)->first();
        
        $validate = [
            'foto' => 'required|image|mimes:jpeg,png,jpg',
            'tanggal' => 'required|max:100',
            'namaKegiatan' => 'required|max:255',
            'deskripsi' => 'required|max:255',
          
        ];

        $image = $request->file('foto');

        if($image == null){
            $imageName = $detail->foto;
            
        }else {
            $image_path = public_path('assets/images/galery/'.$detail->foto);
            if (File::exists($image_path)) {
                unlink($image_path);
            }
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('assets/images/galery'), $imageName);
        }

        $detail->tanggal = $request->get('tanggal');
        $detail->namaKegiatan = $request->get('namaKegiatan');
        $detail->deskripsi = $request->get('deskripsi');
        $detail->foto = $imageName;
        $detail->save();
        \Session::flash('notif-success', 'Update data berhasil');
        return redirect()->route('galeryAdmin.index')->with('success', 'Successfully created.');

    }

    public function delete($id){
        $detail = Galery::where('id', $id)->first();
        $image_path = public_path('assets/images/galery/'.$detail->foto);

        if(!$detail){
            \Session::flash('notif-error', 'Data tidak ditemukan');
            return redirect(route('galeryAdmin.index'));
        }

        if (File::exists($image_path)) {
            unlink($image_path);
        }

        $detail->delete();

        \Session::flash('notif-success', 'Data berhasil dihapus');
        return redirect()->route('galeryAdmin.index')->with('success', 'Designations successfully deleted.');
    }
}
