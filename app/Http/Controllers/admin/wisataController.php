<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wisata;
use App\Repositories\WisataRepository;
use File;

class wisataController extends Controller{

    private $WisataRepository;

    public function __construct(WisataRepository $WisataRepository){

        $this->WisataRepository = $WisataRepository;

    }
   
    public function index(Request $request){

        $page = $request->get('page', 1);
        $limit = 10;

        $wisata = $this->WisataRepository->get($limit);
        $data = [
            'no' => ($page - 1) * $limit,
            'wisata' => $wisata
        ];
        return view('admin/wisata/index',$data);

    }

    public function store(Request $request){

        $validate = [
            'foto' => 'required|image|mimes:jpeg,png,jpg',
            'namaWisata' => 'required|max:100',
            'deskripsi' => 'required|max:255',
          
        ];
        
        $this->validate(request(), $validate);
        $image = $request->file('foto');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('assets/images/wisata'), $imageName);
        $detail = new Wisata();
        $detail->namaWisata = $request->get('namaWisata');
        $detail->deskripsi = $request->get('deskripsi');
        $detail->foto = $imageName;
        $detail->save();
        return redirect()->route('wisataAdmin.index')->with('success', 'Successfully created.');

        

    }

    public function update(Request $request, $id){
        $detail = Wisata::where('id', $id)->first();
        
        $validate = [
            'foto' => 'required|image|mimes:jpeg,png,jpg',
            'namaWisata' => 'required|max:255',
            'deskripsi' => 'required|max:255',
          
        ];

        $image = $request->file('foto');

        if($image == null){
            $imageName = $detail->foto;
            
        }else {
            $image_path = public_path('assets/images/wisata/'.$detail->foto);
            if (File::exists($image_path)) {
                unlink($image_path);
            }
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('assets/images/wisata'), $imageName);
        }

        $detail->namaWisata = $request->get('namaWisata');
        $detail->deskripsi = $request->get('deskripsi');
        $detail->foto = $imageName;
        $detail->save();
        \Session::flash('notif-success', 'Update data berhasil');
        return redirect()->route('wisataAdmin.index')->with('success', 'Successfully created.');

    }

    public function delete($id){
        $detail = Wisata::where('id', $id)->first();
        $image_path = public_path('assets/images/wisata/'.$detail->foto);

        if(!$detail){
            \Session::flash('notif-error', 'Data tidak ditemukan');
            return redirect(route('wisataAdmin.index'));
        }

        if (File::exists($image_path)) {
            unlink($image_path);
        }

        $detail->delete();

        \Session::flash('notif-success', 'Data berhasil dihapus');
        return redirect()->route('wisataAdmin.index')->with('success', 'Designations successfully deleted.');
    }
}
