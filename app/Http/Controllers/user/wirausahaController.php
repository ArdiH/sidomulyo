<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wirausaha;
use App\Repositories\WirausahaRepository;

class wirausahaController extends Controller{

    private $WirausahaRepository;

    public function __construct(WirausahaRepository $WirausahaRepository){

        $this->WirausahaRepository = $WirausahaRepository;

    }

    public function index(Request $request){

        $page = $request->get('page', 1);
        $limit = 10;

        $wirausaha = $this->WirausahaRepository->get($limit);
        $data = [
            'no' => ($page - 1) * $limit,
            'wirausaha' => $wirausaha
        ];

        return view('user/wirausaha/index',$data);

    }
}
