<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Galery;
use App\Repositories\GaleryRepository;

class galeryController extends Controller{

    private $GaleryRepository;

    public function __construct(GaleryRepository $GaleryRepository){

        $this->GaleryRepository = $GaleryRepository;

    }

    public function index(Request $request){

        $page = $request->get('page', 1);
        $limit = 10;

        $galery = $this->GaleryRepository->get($limit);
        $data = [
            'no' => ($page - 1) * $limit,
            'galery' => $galery
        ];

        return view('user/galery/index',$data);
    
    }
}
