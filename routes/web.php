<?php


Auth::routes();

Route::namespace('user')->group(function () {

    Route::get('/', 'homeController@index')->name('user.home');

    Route::group(['prefix' => 'profilDesa', 'as' => 'profilDesa.'], function () {
        Route::get('/', 'profilDesaController@index')->name('index');

    });

    Route::group(['prefix' => 'galery', 'as' => 'galery.'], function () {
        Route::get('/', 'galeryController@index')->name('index');

    });
    
    Route::group(['prefix' => 'wisata', 'as' => 'wisata.'], function () {
        Route::get('/', 'wisataController@index')->name('index');


    });

    Route::group(['prefix' => 'wirausaha', 'as' => 'wirausaha.'], function () {
        Route::get('/', 'wirausahaController@index')->name('index');


    });

    Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
        Route::get('/', 'blogController@index')->name('index');


    });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');

Route::namespace('admin')->middleware('auth:admin')->group(function () {

    Route::get('/adminHome', 'homeController@index')->name('admin.home');

    Route::group(['prefix' => 'galeryAdmin', 'as' => 'galeryAdmin.'], function () {

        Route::get('/', 'galeryController@index')->name('index');
        Route::post('/store', 'galeryController@store')->name('store');
        Route::post('{galeryAdmin}/update', 'galeryController@update')->name('update');
        Route::post('{galeryAdmin}/delete', 'galeryController@delete')->name('delete');


    });

    Route::group(['prefix' => 'wisataAdmin', 'as' => 'wisataAdmin.'], function () {

        Route::get('/', 'wisataController@index')->name('index');
        Route::post('/store', 'wisataController@store')->name('store');
        Route::post('{wisataAdmin}/update', 'wisataController@update')->name('update');
        Route::post('{wisataAdmin}/delete', 'wisataController@delete')->name('delete');


    });

    Route::group(['prefix' => 'wirausahaAdmin', 'as' => 'wirausahaAdmin.'], function () {

        Route::get('/', 'wirausahaController@index')->name('index');
        Route::post('/store', 'wirausahaController@store')->name('store');
        Route::post('{wirausahaAdmin}/update', 'wirausahaController@update')->name('update');
        Route::post('{wirausahaAdmin}/delete', 'wirausahaController@delete')->name('delete');


    });

    Route::group(['prefix' => 'ubahPasswordAdmin', 'as' => 'ubahPasswordAdmin.'], function () {

        Route::get('/', 'ubahPasswordAdminController@index')->name('index');
        Route::post('{userAdmin}/update', 'ubahPasswordAdminController@update')->name('update');


    });


});

